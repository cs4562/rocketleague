import re

def parse(file_path):
    with open(file_path, 'r') as file:
        raw_content = file.read()
    
    lines = raw_content.split('\n')
    cleaned_lines = [line.strip() for line in lines]
    tokens = [tokenize_line(line) for line in cleaned_lines]
    flattened_tokens = [token for sublist in tokens for token in sublist]
    
    current_line = 1
    return parse_program(flattened_tokens, current_line)

def tokenize_line(line):
    token_regex = re.compile(r'[a-zA-Z]+|[0-9]+|[:()=+-]|\$\$')
    return token_regex.findall(line)

def parse_program(tokens, current_line):
    if not tokens:
        return "Accept"
    else:
        result, remaining_tokens = parse_linelist(tokens, current_line)
        if not remaining_tokens:
            return "Accept"
        else:
            return error_message(remaining_tokens[0], current_line)

def parse_linelist(tokens, current_line):
    if not tokens:
        return ('success', ['epsilon']), tokens
    else:
        result_line, new_tokens = parse_line(tokens, current_line)
        if result_line[0] == 'error':
            return result_line, new_tokens
        else:
            result_linelist, new_tokens = parse_linelist(new_tokens, current_line)
            if result_linelist[0] == 'error':
                return result_linelist, new_tokens
            else:
                return ('success', [result_line[1]] + [result_linelist[1]]), new_tokens

def parse_line(tokens, current_line):
    if not tokens:
        return ('error', error_message("Unexpected end of file", current_line)), tokens
    else:
        result_idx, tokens = parse_idx(tokens, current_line)
        if result_idx[0] == 'error':
            return result_idx, tokens
        result_stmt, tokens = parse_stmt(tokens, current_line)
        result_linetail, tokens = parse_linetail(tokens, current_line)
        return ('line', current_line, result_idx[1], result_stmt[1]), tokens

def parse_idx(tokens, current_line):
    if not tokens:
        return ('error', error_message("Unexpected end of file", current_line)), tokens
    current_token = tokens[0]
    if current_token.isdigit() and current_token[0] != '0':
        return ('idx', current_token), tokens[1:]
    else:
        return ('error', error_message("Expected a line number", current_line)), tokens

def parse_linetail(tokens, current_line):
    if not tokens:
        return 'epsilon', tokens
    current_token = tokens[0]
    if current_token == ':':
        return ('linetail', current_token), tokens[1:]
    else:
        return 'epsilon', tokens

def parse_stmt(tokens, current_line):
    # Placeholder for statement parsing logic
    return 'not-implemented', tokens

def error_message(message, line):
    return f"Syntax error: {message} on line {line}"

# Replace 'File01.txt' with the actual file path
result = parse('File01.txt')
print(result)