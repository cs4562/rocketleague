import re

# Token patterns for lexical analysis
token_patterns = {
    'keyword': r'\b(if|while|read|write|goto|gosub|return|break|end|true|false|endwhile)\b',
    'id': r'\b[a-zA-Z][a-zA-Z0-9]*\b',
    'num': r'\b[\+\-]?[0-9]+\b',
    'ops': r'[\+\-\*/]',
    'bool_op': r'<|>|>=|<=|<>|=',
    'punctuation': r'[:;\(\)]',
    'assignment': r'=',
    # Add an 'error' pattern for identifiers starting with '$'
    'error': r'\$\w+',
}

# Compile the patterns into regex objects for efficiency
token_regex = [(key, re.compile(pattern)) for key, pattern in token_patterns.items()]

def tokenize_line(line):
    tokens = []
    position = 0
    while position < len(line):
        match = None
        for token_type, regex in token_regex:
            match = regex.match(line, position)
            if match:
                # Special handling for errors: immediately classify the token as an error
                if token_type == 'error':
                    tokens.append('error')
                    break  # Stop further tokenization of this segment
                else:
                    tokens.append(token_type)
                break
        if match:
            position = match.end()
        else:
            position += 1  # Increment position if no token is matched to avoid infinite loop
    return tokens

def parse_file(filename):
    with open(filename, 'r') as file:
        for lineno, line in enumerate(file, start=1):
            token_types = tokenize_line(line.strip())
            print(f"Line {lineno}: {line.strip()} -> {' '.join(token_types)}")

# Example usage
filename = 'File01.txt'  # Replace this with the path to your file

for file in range(1,6):
    print(f"Processing File0{file}.txt...")
    filename = f'File0{file}.txt'
    parse_file(filename)
