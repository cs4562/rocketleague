#lang racket

(require racket/file
         racket/list
         racket/match
         racket/regex)

(define token-patterns
  (list
   (cons 'keyword  #rx"\\b(if|while|read|write|goto|gosub|return|break|end|true|false|endwhile)\\b")
   (cons 'id       #rx"\\b[a-zA-Z][a-zA-Z0-9]*\\b")
   (cons 'num      #rx"\\b[\\+\\-]?[0-9]+\\b")
   (cons 'ops      #rx"[\\+\\-\\*/]")
   (cons 'bool_op  #rx"<|>|>=|<=|<>|=")
   (cons 'punctuation #rx"[:;\\(\\)]")
   (cons 'assignment  #rx"=")))

(define (tokenize-line line)
  (define (find-token-type token)
    (for/or ((pat token-patterns))
      (match pat
        [(cons type regex)
         (if (regexp-match regex token)
             type
             #f)])))
  (define tokens (regexp-match* #rx"\\S+" line))
  (map find-token-type tokens))

(define (parse-file filename)
  (define lines (file->lines filename))
  (for-each
   (lambda (line)
     (define tokens (tokenize-line line))
     (printf "Line: ~a -> ~a\n" line (string-join (map symbol->string tokens) " ")))
   lines))

; Example usage
(parse-file "File01.txt")
